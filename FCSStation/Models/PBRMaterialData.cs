﻿using System.Collections.Generic;

namespace FCSStation.Models
{
    internal class PBRMaterialData
    {
        /// <summary>
        /// Data that represents the Header
        /// </summary>
        public Dictionary<string,string> GuidAndFriendlyData { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// Data that represents the body of the PBR material
        /// </summary>
        public Dictionary<string,string> FSShaders { get; set; } = new Dictionary<string, string>();
    }
}
