﻿using System.Collections.Generic;
using FCSStation.Enumerators;

namespace FCSStation.Models
{
    public class Cinema4DArgument
    {
        public RunProcessType ProcessType { get; set; }
        public string[] StringArguments { get; set; }

        public Dictionary<string, dynamic> DictArguments { get; set; }

    }
}
