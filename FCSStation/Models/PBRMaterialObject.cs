﻿using System.Collections.Generic;
using FCSStation.Enumerators;

namespace FCSStation.Models
{
    internal class PBRMaterialObject
    {
        public RunProcessType ProcessType { get; set; }
        public string MCXPath { get; set; }
        public string GUID { get; set; }
        public string FriendName { get; set; }
        public string ObjectPath { get; set; }
        public List<Dictionary<string, string>> Arguments {get; set; }
    }
}
