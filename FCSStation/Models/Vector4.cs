﻿namespace FCSStation.Models
{
    internal class Vector4
    {
        public Vector4(double r, double g, double b, double a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }

        public double R { get; set; }
        public double G { get; set; }
        public double B { get; set; }
        public double A { get; set; }
    }
}
