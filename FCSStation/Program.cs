﻿using System;
using System.IO;
using System.Reflection;
using FCSStation.Enumerators;
using FCSStation.Helpers;
using FCSStation.Models;
using FCSStation.Operations;
using Newtonsoft.Json;

namespace FCSStation
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine(Strings.FCStation_Version, GetVersion() ,DateTime.Now, OperatingSystemHelpers.CurrentSystem());

            LogSystem.Initialize();

            //var json = new Cinema4DArgument {ProcessType = RunProcessType.MCX, Arguments = new[]
            //{
            //    @"E:\Flight Simulation Tools\ModelConverter X",
            //    @"Effect-fx_beacon.dae",
            //    @"F:\System Folders\Documents\Cinema4D\Exports"
            //}};

            //var json = new Cinema4DArgument
            //{
            //    ProcessType = RunProcessType.XY2LatLon,
            //    Arguments = new[]
            //    {
            //        @"22.2222222",
            //        @"33.3333333",
            //        @"44.4444444"
            //    }
            //};

            //var json = new Cinema4DArgument
            //{
            //    ProcessType = RunProcessType.FX,
            //    Arguments = null
            //};


            args = new[]
            {
                @"{**ObjectPath**: **F:\\System Folders\\Documents\\Cinema4D\\Exports\\KLAN_C4DTest_4.x**, **FriendName**: **ThisIsATest**, **Arguments**: [{**MetallicTex**: **F:\\System Folders\\Downloads\\klan_hertz_pbr.dds**, **MetallicOcclusion**: 1, **SmoothnessMode**: 1, **SmoothnessValue**: 0.0, **Detail**: 1, **EmissiveTex**: **F:\\System Folders\\Downloads\\KLAN_Hertz_LM.dds**, **AlbedoTex**: **F:\\System Folders\\Downloads\\klan_hertz_diff.dds**, **PrelitVertices**: 0, **NormalTex**: **F:\\System Folders\\Downloads\\klan_hertz_norm_fs.dds**, **EmissiveUV**: 1, **DoubleSided**: 0, **NormalUV**: 0, **AssumeVecticalNormal**: 0, **MaskedThresholdVal**: 0, **AlphaCoverageEnable**: 0, **MaterialName**: **anotherOne**, **MetallicUV**: 1, **DetailScaleY**: 1.0, **DetailScaleX**: 1.0, **ZBias**: 1, **AlbedoDiffCol**: **0.09,1.0,0.363**, **NormalScaleY**: 1.0, **NormalScaleX**: 1.0, **AlbedoUV**: 1, **MetallicValue**: 0.0, **EmissiveMode**: 1, **RenderMode**: 1}, {**MetallicTex**: **F:\\System Folders\\Downloads\\klan_hertz_pbr.dds**, **MetallicOcclusion**: 1, **SmoothnessMode**: 1, **SmoothnessValue**: 0.0, **Detail**: 1, **EmissiveTex**: **F:\\System Folders\\Downloads\\KLAN_Hertz_LM.dds**, **AlbedoTex**: **F:\\System Folders\\Downloads\\klan_hertz_diff.dds**, **PrelitVertices**: 0, **NormalTex**: **F:\\System Folders\\Downloads\\klan_hertz_norm_fs.dds**, **EmissiveUV**: 1, **DoubleSided**: 1, **NormalUV**: 0, **AssumeVecticalNormal**: 1, **MaskedThresholdVal**: 0, **AlphaCoverageEnable**: 0, **MaterialName**: **Mat**, **MetallicUV**: 1, **DetailScaleY**: 1.0, **DetailScaleX**: 1.0, **ZBias**: 2, **AlbedoDiffCol**: **1.0,0.09,0.09**, **NormalScaleY**: 1.0, **NormalScaleX**: 1.0, **AlbedoUV**: 1, **MetallicValue**: 0.0, **EmissiveMode**: 2, **RenderMode**: 1}], **GUID**: **013a89e9-08f4-42b0-a0ad-2e17cf4eebf1**, **ProcessType**: 5, **MCXPath**: **E:\\Flight Simulation Tools\\ModelConverter X**}"
            };

            try
            {
                if (args.Length > 0)
                {
                    Console.WriteLine("Found Arguments");

                    //Console.WriteLine(FixData(args[0]));

                    var data = FixData(args[0]);

                    //File.WriteAllText(@"J:\Documents\Repos\Plugin Development Tools\FCSStation\FCSStation\bin\Debug\netcoreapp3.0\test.json", data);

                    var settings = new JsonSerializerSettings();
                    settings.MissingMemberHandling = MissingMemberHandling.Ignore;

                    var json = JsonConvert.DeserializeObject<PBRMaterialObject>(data);

                    ExecuteOperation(json.ProcessType, json);
                }
            }
            catch (Exception e)
            {
                File.WriteAllText(Path.Combine(ExecutingDirectory(),"error.txt"), $"{e.Message}\n{e.StackTrace}");
                LogSystem.ErrorMessage(e);

            }


            //var json = new Cinema4DArgument
            //{
            //    ProcessType = RunProcessType.PBRXFile,
            //    Arguments = new []{ DummyData.PBRDummyData() }
            //};


            //var json = new Cinema4DArgument
            //{
            //    ProcessType = RunProcessType.XGen,
            //    Arguments = new[]
            //    {
            //        @"F:\System Folders\Documents\Cinema4D\Exports",
            //        @"E:\Flight Simulation Tools\ModelConverter X",
            //        @"F:\Program Files\MAXON\Cinema 4D R19\plugins\FCS ToolKit Plug-in\fcs_toolkit_core_lib\fcs_common_lib\fcs_station_pipeline_engine\ConvertXfile",
            //        @"Untitled 1.x"
            //    }
            //};

            Console.ReadKey();
        }

        private static string FixData(string data)
        {
            return data.Replace("**", "\"").Replace("./",String.Empty);
        }

        /// <summary>
        /// Runs the correct operation based of the <see cref="Cinema4DArgument"/>
        /// </summary>
        /// <param name="json">The <see cref="Cinema4DArgument"/> to get the data from</param>
        private static void ExecuteOperation(RunProcessType processType, object json)
        {
            switch (processType)
            {
                case RunProcessType.MCX:
                    //FlightSimulatorOperations.Cinema4DToMcx((string)json.StringArguments[1], (string)json.StringArguments[2], (string)json.StringArguments[0]);
                    break;
                case RunProcessType.XY2LatLon:
                    //FlightSimulatorOperations.XY2LatLon((string)json.StringArguments[0], (string)json.StringArguments[1], (string)json.StringArguments[2]);
                    break;
                case RunProcessType.Blender:
                    break;
                case RunProcessType.XGen:
                    //FlightSimulatorOperations.CreateXFileWithEffects((string)json.StringArguments[2], (string)json.StringArguments[0], (string)json.StringArguments[3], (string)json.StringArguments[1]);
                    break;
                case RunProcessType.FX:
                    //FlightSimulatorOperations.RetrieveFxFiles();
                    break;
                case RunProcessType.PBRXFile:
                    var data = json as PBRMaterialObject;

                    FlightSimulatorOperations.CreatePBRXFile(data.ObjectPath,data.GUID,data.FriendName,data.Arguments,data.MCXPath);
                    break;
                case RunProcessType.None:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Returns the current version of the application
        /// </summary>
        /// <returns></returns>
        private static string GetVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        /// <summary>
        /// Returns the current executing dictionary of the application
        /// </summary>
        /// <returns></returns>
        internal static string ExecutingDirectory()
        {
            return Environment.CurrentDirectory;
        }
    }
}
