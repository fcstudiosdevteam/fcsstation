﻿using System.Collections.Generic;

namespace FCSStation.Data
{
    internal static class DummyData
    {
        internal static Dictionary<string, dynamic> PBRDummyData()
        {
            var dict = new Dictionary<string, dynamic>()
            {
                {"ObjectPath","F:\\System Folders\\Documents\\Cinema4D\\Exports\\KLAN_Hertz_DiffuseChange.x" },
                {"MCXPATH","E:\\Flight Simulation Tools\\ModelConverter X" },
                {"ProcessType","5" },
                {"GuidAndFriendlyData",new List<Dictionary<string, string>>
                {
                    new Dictionary<string, string>
                    {
                        {"FriendName","ThisIsATest" },
                        {"GUID","08be41b6-2359-480b-2a26-b955a243e9ee" },
                    }
                }},
                {"FSShaders",new List<Dictionary<string, string>>
                {
                    new Dictionary<string, string>
                    {
                        {"MetallicTex","F:\\System Folders\\Downloads\\klan_hertz_pbr.dds" },
                        {"MetallicOcclusion","1" },
                        {"SmoothnessMode","1" },
                        {"SmoothnessValue","0.0" },
                        {"Detail","1.0" },
                        {"EmissiveTex","F:\\System Folders\\Downloads\\KLAN_Hertz_LM.dds" },
                        {"MaskedThresholdVal","4.0" },
                        {"AlbedoTex","F:\\System Folders\\Downloads\\klan_hertz_diff.dds" },
                        {"PrelitVertices","0" },
                        {"NormalTex","F:\\System Folders\\Downloads\\klan_hertz_norm_fs.dds" },
                        {"EmissiveUV","1.0" },
                        {"DoubleSided","1" },
                        {"NormalUV","1.0" },
                        {"AssumeVecticalNormal","0" },
                        {"EmissiveMode","2" },
                        {"MaterialName","Mat" },
                        {"MetallicUV","1.0" },
                        {"DetailScaleY","1.0" },
                        {"DetailScaleX","1.0" },
                        {"ZBias","2.0" },
                        {"AlbedoDiffCol","Vector(1, 0.14, 0.169)" },
                        {"NormalScaleY","1.0" },
                        {"NormalScaleX","1.0" },
                        {"AlbedoUV","1.0" },
                        {"MetallicValue","0.0" },
                        {"AlphaCoverageEnable","0" },
                        {"RenderMode","1" },
                    }
                }},
            };



            return dict;
        }
    }
}
