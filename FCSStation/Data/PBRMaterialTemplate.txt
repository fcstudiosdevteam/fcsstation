﻿PBRMaterial {MATNAME} {
  {RED}; {GREEN}; {BLUE}; {ALPHA};; // Albedo/Diffuse Color  Value
  {METALLICVALUE}; 							// Metallic Value
  {SMOOTHNESSVALUE}; 							// Smoothness Value
  "{RENDERMODE}"; 							// Render Mode / ( 1 is "Opaque"/ 2 is "Masked" / 3 is Translucent ) 
  {THRESHOLDVALUE}; 							// Masked Threshold Value
  {ALPHATOCOVERAGE};									// Alpha To Coverage / ( 1 is "True"/ 0 is "False" )
  {METALLICHASOCCLUSION}; 									// Metallic Has Occlusion / ( 1 is "True"/ 0 is "False" )
  "{SMOOTHNESSSOURCE}"; 						// Smoothness Source / ( 1 is "MetallicAlpha"/ 2 is "AlbedoAlpha" )
  "{EMISSIVEMODE}"; 							// Emissive Mode / ( 1 is "Additive"/ 2 is "AdditiveNightOnly" )
  // Enhanced Parameters
  {ASSUMEVERTICALNORNMAL}; 									// Assume Vectical Normal  / ( 1 is "True"/ 0 is "False" )
  {PRELITVERTICES};									// Prelit Vertices / ( 1 is "True"/ 0 is "False" )
  {DOUBLESIDED};									// Double Sided / ( 1 is "True"/ 0 is "False" )
  {ZBIAS};                					// Z-Bias / Value	
	AlbedoTextureFileName  {
	 "{ALBEDOTEXTURE}";    			// AlbedoTexture
	}
	 MetallicTextureFileName  {
	  "{METALLICTEXTURE}";     		// MetallicTexture
	}
	 NormalTextureFileName  {
	  "{NORMALTEXTURE}";			// NormalTexture
	 }
	 EmissiveTextureFileName  {
	  "{EMISSIVETEXTURE}";				// EmissiveTexture
	}
	AlbedoTextureUVChannel  {
	   {ALBEDOTEXTUREUVCHANNEL};
	}
	MetallicTextureUVChannel  {
	   {METALLICTEXTUREUVCHANNEL};
	}
	NormalTextureUVChannel  {
	   {NORMALTEXTUREUVCHANNEL};
	}
	EmissiveTextureUVChannel  {
	   {EMISSIVETEXTUREUVCHANNEL};
	}
	DetailTextureUVChannel  {
	   {DETAILTEXTUREUVCHANNEL};
	}
	NormalTextureScale  {
	   {NORMALSCALEX};						// NormalScale X  / Value
	   {NORMALSCALEY};    					// NormalScale Y  / Value
	}
	DetailTextureScale  {
	   {DETAILSCALEX};						// DetailScale X  / Value
	   {DETAILSCALEY};						// DetailScale Y  / Value
	}
} // End of PBR Material 