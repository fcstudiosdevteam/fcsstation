﻿using System.Runtime.InteropServices;

namespace FCSStation.Helpers
{
    internal static class OperatingSystemHelpers
    {
        internal static bool IsWindows() =>
            RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

        internal static bool IsMacOS() =>
            RuntimeInformation.IsOSPlatform(OSPlatform.OSX);

        internal static bool IsLinux() =>
            RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

        internal static string CurrentSystem()
        {
            if (IsLinux())
            {
                return "Linux";
            }

            if (IsMacOS())
            {
                return "Mac";
            }

            if (IsWindows())
            {
                return "Windows";
            }

            return "Unknown System";
        }
    }
}
