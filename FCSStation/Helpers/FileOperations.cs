﻿using System;
using System.IO;
using System.Linq;
using FCSStation.Enumerators;
using FCSStation.Operations;

namespace FCSStation.Helpers
{
    internal static class FileOperations
    {
        /// <summary>
        /// Safely gets the name of a file
        /// </summary>
        /// <param name="path">the path of the file to get the name of.</param>
        /// <returns></returns>
        internal static string GetFileName(string path)
        {
            try
            {
                var doesExist = FileExist(path);

                if (doesExist)
                {
                    return Path.GetFileName(path);
                }
            }
            catch (Exception e)
            {
                LogSystem.Message(e.Message, LogType.Error);
            }
            return string.Empty;
        }

        /// <summary>
        /// Safely check if a file exist.
        /// </summary>
        /// <param name="filePath">The path to the file to check for.</param>
        /// <returns></returns>
        internal static bool FileExist(string filePath)
        {
            if(File.Exists(filePath))
            {
                LogSystem.Message($"File {filePath} does exist!");
                return true;
            }
            else
            {
                LogSystem.Message($"File {filePath} doesn't exist!", LogType.Error);
                return false;
            }
             
        }

        /// <summary>
        /// Safely check if a directory exist.
        /// </summary>
        /// <param name="path">The path to the directory to check for.</param>
        /// <returns></returns>
        internal static bool DirectoryExist(string path)
        {
            if (Directory.Exists(path))
            {
                LogSystem.Message($"Directory {path} does exist!");
                return true;
            }
            else
            {
                LogSystem.Message($"Directory {path} doesn't exist!", LogType.Error);
                return false;
            }

        }

        /// <summary>
        /// Copies a file from one location to another safely.
        /// </summary>
        /// <param name="filePath">The path to the file that will be copied.</param>
        /// <param name="destination">The destination to copy the file.</param>
        /// <returns></returns>
        internal static bool CopyFile(string filePath, string destination)
        {
            try
            {
                var doesFExist = FileExist(filePath);
                var doesDExist = DirectoryExist(destination);
                

                if (doesFExist && doesDExist)
                {
                    LogSystem.Message(Strings.ContinuingCopyOperation);

                    //Begin Copying file
                    File.Copy(filePath, Path.Combine(destination,GetFileName(filePath)));

                    LogSystem.Message(Strings.CopyOperationSuccessfull);
                }
                else
                {
                    LogSystem.Message(Strings.CopyFileFailed);
                    return false;
                }
            }
            catch (Exception e)
            {
                LogSystem.Message(e.Message, LogType.Error);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Copies a file from one location to another safely.
        /// </summary>
        /// <param name="filePath">The path to the file that will be copied.</param>
        /// <param name="destination">The destination to copy the file.</param>
        /// <returns></returns>
        internal static bool MoveFile(string filePath, string destination)
        {
            try
            {
                var doesFExist = FileExist(filePath);
                var doesDExist = DirectoryExist(Path.GetDirectoryName(destination));
                
                if (doesFExist && doesDExist)
                {
                    LogSystem.Message(Strings.ContinuingMoveOperation);
                    
                    File.Move(filePath, destination, true);

                    LogSystem.Message(Strings.MoveOperationSuccessfull);
                }
                else
                {
                    LogSystem.Message(Strings.MoveFileFailed);
                    return false;
                }
            }
            catch (Exception e)
            {
                LogSystem.Message(e.Message, LogType.Error);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Removes white spaces and replaces it with the desired string
        /// </summary>
        /// <param name="completeModelPathWSpaces"></param>
        /// <param name="output">The outcome of the remove replace</param>
        /// <param name="filler">What to replace the empty string with.</param>
        /// <returns></returns>
        internal static bool RemoveWhiteSpace(string completeModelPathWSpaces, out string output, string filler = "_")
        {
            output = string.Empty;

            try
            {
                // Check to see if the file path has any spaces
                if ((Path.GetFileName(completeModelPathWSpaces) ?? throw new InvalidOperationException()).Any(char.IsWhiteSpace))
                {
                    // Get the file name...
                    var fileName = Path.GetFileName(completeModelPathWSpaces);
                    // Get the location of the model (DAE) and replace all spaces with underscore...
                    output = fileName.Replace(" ", filler);
                }
                else
                {
                    output = completeModelPathWSpaces;
                }
            }
            catch (Exception e)
            {
                LogSystem.ErrorMessage(e);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Generates the fx text files
        /// </summary>
        /// <param name="flightSimulator"></param>
        /// <param name="exist">returns a bool depending on the file exist</param>
        internal static void GenerateEffectsFile(FlightSimulatorEnum flightSimulator, out bool exist)
        {
            // Create a varable of the effects location in the simulator
            var effectsDirloc = FlightSimulatorOperations.GetLocation(flightSimulator) + @"Effects";

            // Create a varable of the effects text file location in the station folder
            var effectTxtLoc = Path.Combine(Program.ExecutingDirectory(), "FxList", $"{flightSimulator}_Effects.txt");

            // Check if the directory exists
            if (Directory.Exists(effectsDirloc))
            {
                exist = true;
                //check if the effect file already exists. If it does...
                if (File.Exists(effectTxtLoc))
                {
                    // Then delete it.
                    File.Delete(effectTxtLoc);
                }

                //Get all the files in the effects directory
                var d = new DirectoryInfo(effectsDirloc);

                // Find all the file with the extenstion .fx
                var files = d.GetFiles("*.fx");

                // Forech effect...
                foreach (var file in files)
                {
                    // Append it to to the new effect text file.
                    File.AppendAllText(effectTxtLoc, file.Name + Environment.NewLine);
                }
            }
            // if it doesnt exist....
            else
            {
                // return a false
                exist = false;
            }
        }

        internal static void Dispose()
        {
            //TODO Implement
        }

        public static void DeleteFile(string convertXfileLocation)
        {
            try
            {
                if (FileExist(convertXfileLocation))
                {
                    File.Delete(convertXfileLocation);
                }
            }
            catch (Exception e)
            {
                LogSystem.ErrorMessage(e);
            }
        }

        /// <summary>
        /// Creates a directory if it doesn't exist
        /// </summary>
        /// <param name="directoryPath"></param>
        internal static void CreateDirectory(string directoryPath)
        {
            try
            {
                LogSystem.Message(string.Format(Strings.AttemptingToCreateDirectory, directoryPath));

                if (!DirectoryExist(directoryPath))
                {
                    LogSystem.Message(string.Format(Strings.DirectoryDoesntExistCreating, directoryPath));
                    Directory.CreateDirectory(directoryPath);
                    LogSystem.Message(string.Format(Strings.DirectoryCreated, directoryPath));
                }
            }
            catch (Exception e)
            {
                LogSystem.ErrorMessage(e);
            }
        }

        /// <summary>
        /// Fixes file path //|\\ based off the os version
        /// </summary>
        /// <param name="filePath">Path to correct</param>
        /// <returns></returns>
        internal static string FixPath(this string filePath)
        {
            var replacement = Path.DirectorySeparatorChar.ToString();
            if (filePath.Contains("//"))
            {
                return filePath.Replace("//", replacement);
            }

            if (filePath.Contains(@"\\"))
            {
                return filePath.Replace(@"\\", replacement);
            }

            return filePath;
        }
    }
}
