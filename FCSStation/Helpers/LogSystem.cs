﻿using System;
using System.Globalization;
using System.IO;
using System.Xml.Linq;
using FCSStation.Enumerators;

namespace FCSStation.Helpers
{
    public static class LogSystem
    {
        #region Public Properties
        /// <summary>
        /// The current directory for the FCSStation.
        /// </summary>
        public static string FCStationDirectory { get; set; }
        #endregion

        public static void Initialize()
        {
            // Set FCStationDirectory
            FCStationDirectory = AppDomain.CurrentDomain.BaseDirectory;
        }


        #region Public Methods

        /// <summary>
        /// Gets the current date and time
        /// </summary>
        /// <returns></returns>
        public static string CurrentDateTime()
        {
            return DateTime.Now.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// A method to add a message to the messages list
        /// </summary>
        /// <param name="message">The message to show</param>
        /// <param name="loggedDateTime">The date and time the log was entered</param>
        /// <param name="logType">Switches the icon if it is an error message default is None</param>
        /// <param name="isObject">Bool that states if the log has been created from a model export</param>
        /// <param name="modelLocation">Location of the exported object</param>
        public static void Message(string message, string loggedDateTime, LogType logType = LogType.Info, bool isObject = false, string modelLocation = "")
        {
            // Convert the logged date and time to a DateTime
            var dateTime = Convert.ToDateTime(loggedDateTime, CultureInfo.InvariantCulture);

            #region Backups
            //date = DateTime.Date == DateTime.Today ? "Today" : DateTime.ToLongDateString();
            //time = DateTime.Date == DateTime.Today ? DateTimeConverter.TimeAgo(DateTime) : DateTime.ToLongTimeString(); 
            #endregion

            // The current date
            var date = dateTime.ToLongDateString();
            // The current time
            var time = dateTime.ToLongTimeString();

            // Create a message
            LoggerMessage.Date = date;
            LoggerMessage.Time = time;
            LoggerMessage.Message = message;
            LoggerMessage.IsObject = isObject;
            LoggerMessage.ModelLocation = modelLocation;
            LoggerMessage.LogType = logType;

        }

        /// <summary>
        /// A method to add a message to the messages list and gets current date and time then saved
        /// </summary>
        /// <param name="message">The message to show</param>
        /// <param name="logType">Switches the icon if it is an error message default is None</param>
        /// <param name="isObject">Bool that states if the log has been created from a model export</param>
        /// <param name="modelLocation">Location of the exported object</param>
        public static void Message(string message, LogType logType = LogType.Info, bool isObject = false, string modelLocation = "")
        {
            // Create Message
            Message(message, CurrentDateTime(), logType, isObject, modelLocation);

            Console.WriteLine(message);

            //Save log
            SaveLog();
        }

        public static void ErrorMessage(Exception e, string message = "")
        {
            Message($"{message} \n {e.Message} \n {e.StackTrace}");
        }


        #endregion

        /// <summary>
        /// Saves the log to the log xml
        /// </summary>
        public static void SaveLog()
        {
            try
            {
                XDocument xDocument;

                // The current date and time
                var dateTimeStr = $"{LoggerMessage.Date} {LoggerMessage.Time}";
                var dateTime = Convert.ToDateTime(dateTimeStr);
                dateTimeStr = dateTime.ToString(CultureInfo.InvariantCulture);


                var logxmlSaveLocation = Path.Combine(FCStationDirectory, "System", "Log", "FCSStation_logs.xml");


                if (!File.Exists(logxmlSaveLocation))
                {
                    // Create a new Log xml and create the xml structure with new data
                    xDocument = new XDocument(new XElement("FCSStation"));


                    var root = xDocument.Root;

                    root.Add(new XElement("Log",
                        new XAttribute("Message", LoggerMessage.Message),
                        new XAttribute("DateTime", dateTimeStr),
                        new XAttribute("isObject", LoggerMessage.IsObject),
                        new XAttribute("ModelLocation", LoggerMessage.ModelLocation),
                        new XAttribute("LogType", LoggerMessage.LogType)));
                }
                else
                {
                    // Load xml and save new entry
                    xDocument = XDocument.Load(logxmlSaveLocation);
                    var root = xDocument.Root;

                    root.Add(new XElement("Log",
                        new XAttribute("Message", LoggerMessage.Message),
                        new XAttribute("DateTime", dateTimeStr),
                        new XAttribute("isObject", LoggerMessage.IsObject),
                        new XAttribute("ModelLocation", LoggerMessage.ModelLocation),
                        new XAttribute("LogType", LoggerMessage.LogType)));
                }

                xDocument.Save(logxmlSaveLocation);
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.Message}\n{e.StackTrace}");
            }
        }
    }

    /// <summary>
    /// A class that defines a log message
    /// </summary>
    public static class LoggerMessage
    {
        #region Public Properties
        /// <summary>
        /// The date of this message
        /// </summary>
        public static string Date { get; set; }

        /// <summary>
        /// The time of this message
        /// </summary>
        public static string Time { get; set; }

        /// <summary>
        /// The message of this log entry
        /// </summary>
        public static string Message { get; set; }

        /// <summary>
        /// Boolean if log is created from an object export
        /// </summary>
        public static bool IsObject { get; set; }

        /// <summary>
        /// Location of the model if the log is created from an object export
        /// </summary>
        public static string ModelLocation { get; set; }

        /// <summary>
        /// The type of log
        /// </summary>
        public static LogType LogType { get; set; }
        #endregion


    }
}
