﻿using System;
using FCSStation.Models;

namespace FCSStation.Helpers
{
    internal static class HelperOperations
    {
        internal static string UnqiueColorToRGB()
        {
            return String.Empty;
        }

        internal static Vector4 FindUnqiueColor(string s)
        {
            var resultSplit = s.Split(",");

            var vec4 = new Vector4(Convert.ToDouble(resultSplit[0]), Convert.ToDouble(resultSplit[1]),
                Convert.ToDouble(resultSplit[2]), 1.000000);
            
            return vec4;
        }

        internal static string IntToRenderMode(string renderMode)
        {
            switch (renderMode)
            {
                case "1":
                    return "Opaque";
                case "2":
                    return "Masked";
                case "3":
                    return "Translucent";
            }

            return string.Empty;
        }

        public static string IntToSmoothnessSource(string smoothnessSource)
        {
            switch (smoothnessSource)
            {
                case "1":
                    return "MetallicAlpha";
                case "2":
                    return "AlbedoAlpha";
            }

            return string.Empty;
        }

        public static string IntToEmissiveMode(string emissiveMode)
        {
            switch (emissiveMode)
            {
                case "1":
                    return "Additive";
                case "2":
                    return "AdditiveNightOnly";
            }

            return string.Empty;
        }

        public static string ToSixPlaces(this string dec)
        {
            return decimal.TryParse(dec, out decimal d) ? $"{d:N6}" : string.Empty;
        }
    }
}
