﻿namespace FCSStation.Enumerators
{
    /// <summary>
    /// The type  of log
    /// </summary>
    public enum LogType
    {
        /// <summary>
        /// A log that contains a informative message
        /// </summary>
        Info,
        /// <summary>
        /// A log the contains an Model Export
        /// </summary>
        ModelExport,
        /// <summary>
        /// A log the contains an error message
        /// </summary>
        Error,
        /// <summary>
        /// A log that contains a warning message
        /// </summary>
        Warning,
        /// <summary>
        /// A default log
        /// </summary>
        None
    }
}
