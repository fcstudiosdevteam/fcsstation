﻿namespace FCSStation.Enumerators
{
    /// <summary>
    /// They types of run commands for the station
    /// </summary>
    public enum RunProcessType
    {
        /// <summary>
        /// Model Converter X
        /// </summary>
        MCX,
        /// <summary>
        /// Blender
        /// </summary>
        Blender,
        /// <summary>
        /// X File Generator
        /// </summary>
        XGen,
        /// <summary>
        /// XY2LatLon
        /// </summary>
        XY2LatLon,
        /// <summary>
        /// Effect Files
        /// </summary>
        FX,
        /// <summary>
        /// A PBR XFile Export
        /// </summary>
        PBRXFile,
        /// <summary>
        /// A Default Value
        /// </summary>
        None

    }
}
