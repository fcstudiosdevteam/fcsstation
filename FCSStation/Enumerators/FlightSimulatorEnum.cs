﻿namespace FCSStation.Enumerators
{
    /// <summary>
    /// Enums of the different flight simualors
    /// </summary>
    public enum FlightSimulatorEnum
    {
        /// <summary>
        /// Flight Simulator 2004
        /// </summary>
        FS2004,
        /// <summary>
        /// Flight Simualtor X
        /// </summary>
        FSX,
        /// <summary>
        /// Flight Simulator Steam Edition
        /// </summary>
        FSXSE,
        /// <summary>
        /// Prepar3D V1
        /// </summary>
        P3DV1,
        /// <summary>
        /// Prepar3D V2
        /// </summary>
        P3DV2,
        /// <summary>
        /// Prepar3D V3
        /// </summary>
        P3DV3,
        /// <summary>
        /// Prepar3D V4
        /// </summary>
        P3DV4
    }
}
