﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using FCSStation.Enumerators;
using FCSStation.Helpers;
using Microsoft.Win32;
using TextCopy;

namespace FCSStation.Operations
{
    internal static class FlightSimulatorOperations
    {
        /// <summary>
        /// Sends a file to ModelConverter X and launches the application in a safe manner.
        /// </summary>
        /// <param name="modelName">The name of the model to send to ModelConverter X.</param>
        /// <param name="modelLocation">The location of the model to send to ModelConverter X.</param>
        /// <param name="modelConverterX">The location of Model Converter X on the system.</param>
        /// <returns></returns>
        internal static bool Cinema4DToMcx(string modelName, string modelLocation, string modelConverterX)
        {
            var isSuccessful = false;
            try
            {

                //Use path combine to create the folder location
                var fileOLocation = Path.Combine(modelLocation, modelName);

                //Lets check if the path exist

                var doesExist = FileOperations.FileExist(fileOLocation);

                if (doesExist)
                {
                    //Remove white spaces
                    if (FileOperations.RemoveWhiteSpace(modelName, out modelName))
                    {
                        //Use path combine to create the folder location
                        var fileLocation = Path.Combine(modelLocation, modelName);

                        if (FileOperations.MoveFile(fileOLocation, fileLocation))
                        {
                            // Create a process for MCX
                            var pi = new ProcessStartInfo(fileLocation)
                            {
                                Arguments = Path.GetFileName(fileLocation) ?? throw new InvalidOperationException(),
                                UseShellExecute = true,
                                WorkingDirectory =
                                    Path.GetDirectoryName(fileLocation) ?? throw new InvalidOperationException(),
                                FileName = Path.Combine(modelConverterX, "ModelConverterX.exe"),
                                Verb = "OPEN"
                            };

                            // Start MCX
                            var p = new Process { StartInfo = pi };

                            // holds if the application started successfully
                            var successfulStart = p.Start();

                            // Delete all files that the station used
                            FileOperations.Dispose();

                            // If we had a successful start
                            if (successfulStart)
                            {
                                // Notify the person of the start
                                LogSystem.Message(
                                    string.Format(Strings.MCXStartedSuccessfull, Path.GetFileName(fileLocation)),
                                    LogType.ModelExport, true, fileLocation);
                                isSuccessful = true;
                            }
                            else
                            {
                                // Send a message to the log in the main ui
                                LogSystem.Message(Strings.MCXStartedFailed, LogType.Error);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LogSystem.ErrorMessage(e, Strings.OperationFailedMessage);
            }

            return isSuccessful;
        }

        /// <summary>
        /// Does a copy to clipboard for the XY2LatLong
        /// </summary>
        /// <param name="xy2Lat">The latitude information.</param>
        /// <param name="xy2Lon">The l information.</param>
        /// <param name="xy2Alt">The latitude information.</param>
        internal static bool XY2LatLon(string xy2Lat, string xy2Lon, string xy2Alt)
        {
            try
            {
                Clipboard.SetText($"Lat = {xy2Lat}, Lon = {xy2Lon}, Alt = {xy2Alt}");

                LogSystem.Message("FCS Station has copied the coordinates to your clipboard!");

                // Delete all temp data
                FileOperations.Dispose();

                LogSystem.Message(Strings.XY2LatLonSuccessfull);
            }
            catch (Exception e)
            {
                LogSystem.ErrorMessage(e, Strings.XY2LatLonFailed);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Retrieves the location of the Flight Simulator from the registry
        /// </summary>
        /// <returns><see cref="string"/></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>     
        internal static string GetLocation(FlightSimulatorEnum simulatorEnum)
        {
            try
            {
                if (OperatingSystemHelpers.IsWindows())
                {
                    switch (simulatorEnum)
                    {
                        case FlightSimulatorEnum.FS2004:
                            using (var fs2004Key =
                                Registry.LocalMachine.OpenSubKey(
                                    @"SOFTWARE\WOW6432Node\Microsoft\microsoft games\flight simulator\9.0"))
                            {
                                if (fs2004Key != null)
                                {
                                    return $@"{fs2004Key.GetValue("EXE Path")}\";
                                }
                            }

                            break;

                        case FlightSimulatorEnum.FSX:
                            using (var fsxKey =
                                Registry.CurrentUser.OpenSubKey(
                                    @"SOFTWARE\Microsoft\Microsoft Games\Flight Simulator\10.0"))
                            {
                                if (fsxKey != null)
                                {
                                    return fsxKey.GetValue("AppPath").ToString();
                                }
                            }

                            break;

                        case FlightSimulatorEnum.FSXSE:
                            using (var seKey = Registry.CurrentUser.OpenSubKey(
                                @"SOFTWARE\Microsoft\Microsoft Games\Flight Simulator - Steam Edition\10.0"))
                            {
                                if (seKey != null)
                                {
                                    return seKey.GetValue("AppPath").ToString();
                                }
                            }

                            break;

                        case FlightSimulatorEnum.P3DV1:
                            using (var p3Dv1Key =
                                Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Lockheed Martin\Prepar3D v1"))
                            {
                                if (p3Dv1Key != null)
                                {
                                    return p3Dv1Key.GetValue("AppPath").ToString();
                                }
                            }

                            break;

                        case FlightSimulatorEnum.P3DV2:
                            using (var p3Dv2Key =
                                Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Lockheed Martin\Prepar3D v2"))
                            {
                                if (p3Dv2Key != null)
                                {
                                    return p3Dv2Key.GetValue("AppPath").ToString();
                                }
                            }

                            break;

                        case FlightSimulatorEnum.P3DV3:
                            using (var p3Dv3Key =
                                Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Lockheed Martin\Prepar3D v3"))
                            {
                                if (p3Dv3Key != null)
                                {
                                    return p3Dv3Key.GetValue("AppPath").ToString();
                                }
                            }

                            break;

                        case FlightSimulatorEnum.P3DV4:
                            using (var p3Dv4Key =
                                Registry.CurrentUser.OpenSubKey(@"Software\Lockheed Martin\Prepar3D v4"))
                            {
                                if (p3Dv4Key != null)
                                {
                                    return StripInvalidCharsFromRegistryStr(p3Dv4Key.GetValue("AppPath").ToString());

                                }
                            }

                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(simulatorEnum), simulatorEnum, null);
                    }
                }
            }
            catch (Exception e)
            {
                LogSystem.ErrorMessage(e);
            }

            // If no condition is met return string empty
            return string.Empty;
        }

        /// <summary>
        /// Retrieves the FX files from the Flight Simulators
        /// </summary>
        internal static void RetrieveFxFiles()
        {
            try
            {
                // Make a new list to store the ammount of sims added
                var simsCreated = new List<string>();

                // If the Fx directory doesnt exist lets create it
                FileOperations.CreateDirectory(Path.Combine(Program.ExecutingDirectory(), "FxList"));

                // Get the amount of enums inthe FlightSimulatorEnum enum
                var fsEnumCount = Enum.GetNames(typeof(FlightSimulatorEnum)).Length;

                // Create the effects file
                for (int i = 0; i < fsEnumCount; i++)
                {
                    // Generate the effects file
                    FileOperations.GenerateEffectsFile((FlightSimulatorEnum)i, out var exist);
                    if (exist)
                    {
                        simsCreated.Add(((FlightSimulatorEnum)i).ToString());
                    }
                }

                if (simsCreated.Count > 0)
                {
                    LogSystem.Message($"FCS Station has generate the effects from: {GenerateAddedEffect(simsCreated)}");
                }
                else
                {
                    LogSystem.Message($"No flight simulator found!", LogType.Error);
                }

                FileOperations.Dispose();
            }
            catch (Exception e)
            {
                LogSystem.ErrorMessage(e);
                FileOperations.Dispose();
            }
        }

        /// <summary>
        /// Generates an XFile with effects
        /// </summary>
        /// <param name="exportLocation">Where to export the generated Xfile</param>
        /// <param name="modelLocation">The location of the XFile</param>
        /// <param name="modelFileName">The file name of the XFile</param>
        /// <param name="modelConverterXLocation">The path to ModelConverterX</param>
        /// <param name="exportXFileToMCX">Wither to export the file to ModelConverterX</param>
        internal static void CreateXFileWithEffects(string exportLocation, string modelLocation, string modelFileName,
            string modelConverterXLocation, bool exportXFileToMCX = true)
        {
            var convertXfileLocation = Path.Combine(modelLocation, modelFileName);
            int endOfHeaderIndex = 0;

            try
            {
                // Read the source x file
                var sourceXfile = File.ReadAllLines(convertXfileLocation).ToList();

                // Read the part data file
                var partTxtFile = File.ReadAllLines(Path.Combine(Program.ExecutingDirectory(), "C4DFXs_Xfile.txt"));

                // For each descendant of the source x file
                for (int i = 0; i < sourceXfile.Count; i++)
                {
                    // find the header ...
                    if (sourceXfile[i].Trim().Equals("Header {"))
                    {
                        for (int j = i; j < sourceXfile.Count; j++)
                        {
                            // find the closing bracket...
                            if (sourceXfile[j] == "}")
                            {
                                // go to the next line after the header and get the elements
                                endOfHeaderIndex = j + 1;
                                goto End;
                            }
                        }
                    }
                }
            End:

                //Insert the partFile Data
                for (int i = partTxtFile.Length - 1; i > -1; i--)
                {
                    sourceXfile.Insert(endOfHeaderIndex, partTxtFile[i]);
                }

                var lastIndex = sourceXfile.Count - 1;

                // Finish off the XML with the following lines
                sourceXfile.Insert(lastIndex, @"}    // End of frm-MasterScale frame");
                sourceXfile.Insert(lastIndex, @"}    // End of frm-MasterUnitConversion frame");
                sourceXfile.Insert(lastIndex, @"}    // End of RotateAroundX frame");

                // Delete the file in the ConvertX Directory to prevent the StreamWriter from appending to the file
                // and causing the xfile to contain the 3dModel
                if (FileOperations.FileExist(convertXfileLocation))
                    FileOperations.DeleteFile(convertXfileLocation);

                // for each descendant of line in the source x file                             
                foreach (var line in sourceXfile)
                {
                    // Add the element value
                    using (var tw = new StreamWriter(convertXfileLocation, true))
                    {
                        //Append line to the file
                        tw.WriteLine(line);
                    }
                }

                // Set the Export File location
                var exportFileLocation = Path.Combine(exportLocation, modelFileName);

                //If Export file to mcx is true
                if (exportXFileToMCX)
                {
                    // Replace all white spaces and run ExportToMCX
                    Cinema4DToMcx(modelFileName, modelLocation, modelConverterXLocation);

                    // Send message to user.
                    LogSystem.Message(string.Format(Strings.XfileExportedAndMCXLaunched, modelFileName),
                        LogType.ModelExport, true, exportLocation);
                }
                else
                {
                    //Copy the XFile from the ConvertXFile location and override the file if it exist
                    FileOperations.CopyFile(convertXfileLocation, exportFileLocation);
                    // Send message to user
                    LogSystem.Message(string.Format(Strings.XfileExported, convertXfileLocation), LogType.ModelExport,
                        true, exportFileLocation);
                }

                // Delete all temp data
                FileOperations.Dispose();
            }
            catch (Exception e)
            {
                // Send message to user
                LogSystem.ErrorMessage(e);
            }
        }

        internal static void CreatePBRXFile(string modelLocation, string guid, string friendlyName,
            List<Dictionary<string, string>> fssShader, string modelConverterXLocation)
        {
            try
            {
                List<string> xFile;

                if (FileOperations.FileExist(modelLocation))
                {
                    xFile = File.ReadAllLines(modelLocation).ToList();
                }
                else
                {
                    return;
                }

                var guidTemplate = Strings.GuidToNameTemplate
                    .Replace("{GUID}", guid)
                    .Replace("{FRIENDLYNAME}", friendlyName);

                string pbrParameters = null;
                foreach (var shader in fssShader)
                {
                    var vect = HelperOperations.FindUnqiueColor(shader["AlbedoDiffCol"]);

                    pbrParameters = Strings.PBRMaterialTemplate

                        //Textures
                        .Replace("{METALLICTEXTURE}", shader["MetallicTex"].FixPath())
                        .Replace("{ALBEDOTEXTURE}", shader["AlbedoTex"].FixPath())
                        .Replace("{NORMALTEXTURE}", shader["NormalTex"].FixPath())
                        .Replace("{EMISSIVETEXTURE}", shader["EmissiveTex"].FixPath())

                        //Diffuse Color
                        .Replace("{RED}", vect.R.ToString("N6"))
                        .Replace("{GREEN}", vect.G.ToString("N6"))
                        .Replace("{BLUE}", vect.B.ToString("N6"))
                        .Replace("{ALPHA}", vect.A.ToString("N6"))

                        //UV Channels
                        .Replace("{NORMALTEXTUREUVCHANNEL}", shader["NormalUV"])
                        .Replace("{ALBEDOTEXTUREUVCHANNEL}", shader["AlbedoUV"])
                        .Replace("{METALLICTEXTUREUVCHANNEL}", shader["MetallicUV"])
                        .Replace("{DETAILTEXTUREUVCHANNEL}", shader["Detail"])
                        .Replace("{EMISSIVETEXTUREUVCHANNEL}", shader["EmissiveUV"])
                    
                        //Values
                        .Replace("{SMOOTHNESSVALUE}", shader["SmoothnessValue"].ToSixPlaces())
                        .Replace("{METALLICVALUE}", shader["MetallicValue"].ToSixPlaces())
                        .Replace("{THRESHOLDVALUE}", shader["MaskedThresholdVal"].ToSixPlaces())
                        .Replace("{ZBIAS}", shader["ZBias"])

                        //Modes
                        .Replace("{EMISSIVEMODE}", HelperOperations.IntToEmissiveMode(shader["EmissiveMode"]))
                        .Replace("{SMOOTHNESSSOURCE}", HelperOperations.IntToSmoothnessSource(shader["SmoothnessMode"]))
                        .Replace("{RENDERMODE}", HelperOperations.IntToRenderMode(shader["RenderMode"]))

                        //Booleans
                        .Replace("{METALLICHASOCCLUSION}", shader["MetallicOcclusion"])
                        .Replace("{PRELITVERTICES}", shader["PrelitVertices"])
                        .Replace("{DOUBLESIDED}", shader["DoubleSided"])
                        .Replace("{ASSUMEVERTICALNORNMAL}", shader["AssumeVecticalNormal"])
                        .Replace("{ALPHATOCOVERAGE}", shader["AlphaCoverageEnable"])

                        //Material Name
                        .Replace("{MATNAME}", shader["MaterialName"])

                        //Scales
                        .Replace("{DETAILSCALEY}", shader["DetailScaleY"].ToSixPlaces())
                        .Replace("{DETAILSCALEX}", shader["DetailScaleX"].ToSixPlaces())
                        .Replace("{NORMALSCALEY}", shader["NormalScaleY"].ToSixPlaces())
                        .Replace("{NORMALSCALEX}", shader["NormalScaleX"].ToSixPlaces());
                
                    xFile = RemoveC4DMaterial(shader["MaterialName"], xFile);

                    // For each descendant of the source x file
                    int endOfHeaderIndex = 0;
                    for (int i = 0; i < xFile.Count; i++)
                    {
                        // find the header ...
                        if (xFile[i].Trim().Equals("Header {"))
                        {
                            endOfHeaderIndex = i - 1;
                        }
                    }

                    //Insert the partFile Data
                    xFile.Insert(endOfHeaderIndex, guidTemplate);

                    // For each descendant of the source x file
                    int endOfMeshMaterialList = 0;
                    for (int i = 0; i < xFile.Count; i++)
                    {
                        // find the header ...
                        if (xFile[i].Trim().Equals("Material C4DMAT_NONE {"))
                        {
                            for (int j = i; j < xFile.Count; j++)
                            {
                                // find the closing bracket...
                                if (xFile[j] == "    ")
                                {
                                    // go to the next line after the header and get the elements
                                    endOfMeshMaterialList = j + 1;
                                    goto End2;
                                }
                            }
                        }
                    }

                End2:

                    //Insert the partFile Data
                    xFile.Insert(endOfMeshMaterialList, pbrParameters);

                    // Delete the file in the ConvertX Directory to prevent the StreamWriter from appending to the file
                    // and causing the xfile to contain the 3dModel

                    if (FileOperations.FileExist(modelLocation))
                        FileOperations.DeleteFile(modelLocation);

                    // for each descendant of line in the source x file                             
                    foreach (var line in xFile)
                    {
                        // Add the element value
                        using (var tw = new StreamWriter(modelLocation, true))
                        {
                            //Append line to the file
                            tw.WriteLine(line);
                        }
                    }

                }


                // Replace all white spaces and run ExportToMCX
                Cinema4DToMcx(Path.GetFileName(modelLocation), Path.GetDirectoryName(modelLocation), modelConverterXLocation);
            }
            catch (Exception e)
            {
                LogSystem.ErrorMessage(e);
            }
        }

        private static List<string> RemoveC4DMaterial(string matName, List<string> xFile)
        {
            var targetLine = 0;

            for (int i = 0; i < xFile.Count; i++)
            {
                if (xFile[i].Trim().StartsWith($"Material C4DMAT_{matName}"))
                {
                    targetLine = i;
                    break;
                }
            }
            
            while (xFile[targetLine].Trim() != "}")
            {
                xFile.RemoveAt(targetLine);
            }

            xFile.RemoveAt(targetLine);

            return xFile;
        }
        

        /// <summary>
        /// Returns a string that contains all the effects that where created
        /// </summary>
        /// <param name="effectsList">List of effects</param>
        /// <returns>A <see cref="string" with all the added effects/></returns>
        private static string GenerateAddedEffect(List<string> effectsList)
        {
            var effectString = new StringBuilder();

            for (var i = 0; i < effectsList.Count; i++)
            {
                effectString.Append(i == effectsList.Count - 1 ? $"{effectsList[i]}" : $"{effectsList[i]}, ");
            }

            return effectString.ToString();
        }

        /// <summary>
        /// Removes the invalid charaters from the registry string
        /// </summary>
        /// <param name="str">The registry string to correct</param>
        /// <returns></returns>
        private static string StripInvalidCharsFromRegistryStr(string str)
        {
            if (str.Contains(Convert.ToChar(0x0).ToString()))
            {
                str = str.Substring(0, str.IndexOf(Convert.ToChar(0x0).ToString(), StringComparison.Ordinal));
            }

            return str;
        }

    }
}
